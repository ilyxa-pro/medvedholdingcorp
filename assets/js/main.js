/*! WOW - v1.1.2 - 2015-08-19
* Copyright (c) 2015 Matthieu Aussaguel; Licensed MIT */
(function(){var a,b,c,d,e,f=function(a,b){return function(){return a.apply(b,arguments)}},g=[].indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(b in this&&this[b]===a)return b;return-1};b=function(){function a(){}return a.prototype.extend=function(a,b){var c,d;for(c in b)d=b[c],null==a[c]&&(a[c]=d);return a},a.prototype.isMobile=function(a){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(a)},a.prototype.createEvent=function(a,b,c,d){var e;return null==b&&(b=!1),null==c&&(c=!1),null==d&&(d=null),null!=document.createEvent?(e=document.createEvent("CustomEvent"),e.initCustomEvent(a,b,c,d)):null!=document.createEventObject?(e=document.createEventObject(),e.eventType=a):e.eventName=a,e},a.prototype.emitEvent=function(a,b){return null!=a.dispatchEvent?a.dispatchEvent(b):b in(null!=a)?a[b]():"on"+b in(null!=a)?a["on"+b]():void 0},a.prototype.addEvent=function(a,b,c){return null!=a.addEventListener?a.addEventListener(b,c,!1):null!=a.attachEvent?a.attachEvent("on"+b,c):a[b]=c},a.prototype.removeEvent=function(a,b,c){return null!=a.removeEventListener?a.removeEventListener(b,c,!1):null!=a.detachEvent?a.detachEvent("on"+b,c):delete a[b]},a.prototype.innerHeight=function(){return"innerHeight"in window?window.innerHeight:document.documentElement.clientHeight},a}(),c=this.WeakMap||this.MozWeakMap||(c=function(){function a(){this.keys=[],this.values=[]}return a.prototype.get=function(a){var b,c,d,e,f;for(f=this.keys,b=d=0,e=f.length;e>d;b=++d)if(c=f[b],c===a)return this.values[b]},a.prototype.set=function(a,b){var c,d,e,f,g;for(g=this.keys,c=e=0,f=g.length;f>e;c=++e)if(d=g[c],d===a)return void(this.values[c]=b);return this.keys.push(a),this.values.push(b)},a}()),a=this.MutationObserver||this.WebkitMutationObserver||this.MozMutationObserver||(a=function(){function a(){"undefined"!=typeof console&&null!==console&&console.warn("MutationObserver is not supported by your browser."),"undefined"!=typeof console&&null!==console&&console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")}return a.notSupported=!0,a.prototype.observe=function(){},a}()),d=this.getComputedStyle||function(a){return this.getPropertyValue=function(b){var c;return"float"===b&&(b="styleFloat"),e.test(b)&&b.replace(e,function(a,b){return b.toUpperCase()}),(null!=(c=a.currentStyle)?c[b]:void 0)||null},this},e=/(\-([a-z]){1})/g,this.WOW=function(){function e(a){null==a&&(a={}),this.scrollCallback=f(this.scrollCallback,this),this.scrollHandler=f(this.scrollHandler,this),this.resetAnimation=f(this.resetAnimation,this),this.start=f(this.start,this),this.scrolled=!0,this.config=this.util().extend(a,this.defaults),null!=a.scrollContainer&&(this.config.scrollContainer=document.querySelector(a.scrollContainer)),this.animationNameCache=new c,this.wowEvent=this.util().createEvent(this.config.boxClass)}return e.prototype.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:null,scrollContainer:null},e.prototype.init=function(){var a;return this.element=window.document.documentElement,"interactive"===(a=document.readyState)||"complete"===a?this.start():this.util().addEvent(document,"DOMContentLoaded",this.start),this.finished=[]},e.prototype.start=function(){var b,c,d,e;if(this.stopped=!1,this.boxes=function(){var a,c,d,e;for(d=this.element.querySelectorAll("."+this.config.boxClass),e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.all=function(){var a,c,d,e;for(d=this.boxes,e=[],a=0,c=d.length;c>a;a++)b=d[a],e.push(b);return e}.call(this),this.boxes.length)if(this.disabled())this.resetStyle();else for(e=this.boxes,c=0,d=e.length;d>c;c++)b=e[c],this.applyStyle(b,!0);return this.disabled()||(this.util().addEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().addEvent(window,"resize",this.scrollHandler),this.interval=setInterval(this.scrollCallback,50)),this.config.live?new a(function(a){return function(b){var c,d,e,f,g;for(g=[],c=0,d=b.length;d>c;c++)f=b[c],g.push(function(){var a,b,c,d;for(c=f.addedNodes||[],d=[],a=0,b=c.length;b>a;a++)e=c[a],d.push(this.doSync(e));return d}.call(a));return g}}(this)).observe(document.body,{childList:!0,subtree:!0}):void 0},e.prototype.stop=function(){return this.stopped=!0,this.util().removeEvent(this.config.scrollContainer||window,"scroll",this.scrollHandler),this.util().removeEvent(window,"resize",this.scrollHandler),null!=this.interval?clearInterval(this.interval):void 0},e.prototype.sync=function(){return a.notSupported?this.doSync(this.element):void 0},e.prototype.doSync=function(a){var b,c,d,e,f;if(null==a&&(a=this.element),1===a.nodeType){for(a=a.parentNode||a,e=a.querySelectorAll("."+this.config.boxClass),f=[],c=0,d=e.length;d>c;c++)b=e[c],g.call(this.all,b)<0?(this.boxes.push(b),this.all.push(b),this.stopped||this.disabled()?this.resetStyle():this.applyStyle(b,!0),f.push(this.scrolled=!0)):f.push(void 0);return f}},e.prototype.show=function(a){return this.applyStyle(a),a.className=a.className+" "+this.config.animateClass,null!=this.config.callback&&this.config.callback(a),this.util().emitEvent(a,this.wowEvent),this.util().addEvent(a,"animationend",this.resetAnimation),this.util().addEvent(a,"oanimationend",this.resetAnimation),this.util().addEvent(a,"webkitAnimationEnd",this.resetAnimation),this.util().addEvent(a,"MSAnimationEnd",this.resetAnimation),a},e.prototype.applyStyle=function(a,b){var c,d,e;return d=a.getAttribute("data-wow-duration"),c=a.getAttribute("data-wow-delay"),e=a.getAttribute("data-wow-iteration"),this.animate(function(f){return function(){return f.customStyle(a,b,d,c,e)}}(this))},e.prototype.animate=function(){return"requestAnimationFrame"in window?function(a){return window.requestAnimationFrame(a)}:function(a){return a()}}(),e.prototype.resetStyle=function(){var a,b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(a.style.visibility="visible");return e},e.prototype.resetAnimation=function(a){var b;return a.type.toLowerCase().indexOf("animationend")>=0?(b=a.target||a.srcElement,b.className=b.className.replace(this.config.animateClass,"").trim()):void 0},e.prototype.customStyle=function(a,b,c,d,e){return b&&this.cacheAnimationName(a),a.style.visibility=b?"hidden":"visible",c&&this.vendorSet(a.style,{animationDuration:c}),d&&this.vendorSet(a.style,{animationDelay:d}),e&&this.vendorSet(a.style,{animationIterationCount:e}),this.vendorSet(a.style,{animationName:b?"none":this.cachedAnimationName(a)}),a},e.prototype.vendors=["moz","webkit"],e.prototype.vendorSet=function(a,b){var c,d,e,f;d=[];for(c in b)e=b[c],a[""+c]=e,d.push(function(){var b,d,g,h;for(g=this.vendors,h=[],b=0,d=g.length;d>b;b++)f=g[b],h.push(a[""+f+c.charAt(0).toUpperCase()+c.substr(1)]=e);return h}.call(this));return d},e.prototype.vendorCSS=function(a,b){var c,e,f,g,h,i;for(h=d(a),g=h.getPropertyCSSValue(b),f=this.vendors,c=0,e=f.length;e>c;c++)i=f[c],g=g||h.getPropertyCSSValue("-"+i+"-"+b);return g},e.prototype.animationName=function(a){var b;try{b=this.vendorCSS(a,"animation-name").cssText}catch(c){b=d(a).getPropertyValue("animation-name")}return"none"===b?"":b},e.prototype.cacheAnimationName=function(a){return this.animationNameCache.set(a,this.animationName(a))},e.prototype.cachedAnimationName=function(a){return this.animationNameCache.get(a)},e.prototype.scrollHandler=function(){return this.scrolled=!0},e.prototype.scrollCallback=function(){var a;return!this.scrolled||(this.scrolled=!1,this.boxes=function(){var b,c,d,e;for(d=this.boxes,e=[],b=0,c=d.length;c>b;b++)a=d[b],a&&(this.isVisible(a)?this.show(a):e.push(a));return e}.call(this),this.boxes.length||this.config.live)?void 0:this.stop()},e.prototype.offsetTop=function(a){for(var b;void 0===a.offsetTop;)a=a.parentNode;for(b=a.offsetTop;a=a.offsetParent;)b+=a.offsetTop;return b},e.prototype.isVisible=function(a){var b,c,d,e,f;return c=a.getAttribute("data-wow-offset")||this.config.offset,f=this.config.scrollContainer&&this.config.scrollContainer.scrollTop||window.pageYOffset,e=f+Math.min(this.element.clientHeight,this.util().innerHeight())-c,d=this.offsetTop(a),b=d+a.clientHeight,e>=d&&b>=f},e.prototype.util=function(){return null!=this._util?this._util:this._util=new b},e.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},e}()}).call(this);

if ('objectFit' in document.documentElement.style === false) {
  var objectFitImages = function () { "use strict"; function t(t, e) { return "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='" + t + "' height='" + e + "'%3E%3C/svg%3E" } function e(t) { if (t.srcset && !p && window.picturefill) { var e = window.picturefill._; t[e.ns] && t[e.ns].evaled || e.fillImg(t, { reselect: !0 }), t[e.ns].curSrc || (t[e.ns].supported = !1, e.fillImg(t, { reselect: !0 })), t.currentSrc = t[e.ns].curSrc || t.src } } function i(t) { for (var e, i = getComputedStyle(t).fontFamily, r = {}; null !== (e = u.exec(i));)r[e[1]] = e[2]; return r } function r(e, i, r) { var n = t(i || 1, r || 0); b.call(e, "src") !== n && h.call(e, "src", n) } function n(t, e) { t.naturalWidth ? e(t) : setTimeout(n, 100, t, e) } function c(t) { var c = i(t), o = t[l]; if (c["object-fit"] = c["object-fit"] || "fill", !o.img) { if ("fill" === c["object-fit"]) return; if (!o.skipTest && f && !c["object-position"]) return } if (!o.img) { o.img = new Image(t.width, t.height), o.img.srcset = b.call(t, "data-ofi-srcset") || t.srcset, o.img.src = b.call(t, "data-ofi-src") || t.src, h.call(t, "data-ofi-src", t.src), t.srcset && h.call(t, "data-ofi-srcset", t.srcset), r(t, t.naturalWidth || t.width, t.naturalHeight || t.height), t.srcset && (t.srcset = ""); try { s(t) } catch (t) { window.console && console.warn("https://bit.ly/ofi-old-browser") } } e(o.img), t.style.backgroundImage = 'url("' + (o.img.currentSrc || o.img.src).replace(/"/g, '\\"') + '")', t.style.backgroundPosition = c["object-position"] || "center", t.style.backgroundRepeat = "no-repeat", t.style.backgroundOrigin = "content-box", /scale-down/.test(c["object-fit"]) ? n(o.img, function () { o.img.naturalWidth > t.width || o.img.naturalHeight > t.height ? t.style.backgroundSize = "contain" : t.style.backgroundSize = "auto" }) : t.style.backgroundSize = c["object-fit"].replace("none", "auto").replace("fill", "100% 100%"), n(o.img, function (e) { r(t, e.naturalWidth, e.naturalHeight) }) } function s(t) { var e = { get: function (e) { return t[l].img[e ? e : "src"] }, set: function (e, i) { return t[l].img[i ? i : "src"] = e, h.call(t, "data-ofi-" + i, e), c(t), e } }; Object.defineProperty(t, "src", e), Object.defineProperty(t, "currentSrc", { get: function () { return e.get("currentSrc") } }), Object.defineProperty(t, "srcset", { get: function () { return e.get("srcset") }, set: function (t) { return e.set(t, "srcset") } }) } function o() { function t(t, e) { return t[l] && t[l].img && ("src" === e || "srcset" === e) ? t[l].img : t } d || (HTMLImageElement.prototype.getAttribute = function (e) { return b.call(t(this, e), e) }, HTMLImageElement.prototype.setAttribute = function (e, i) { return h.call(t(this, e), e, String(i)) }) } function a(t, e) { var i = !y && !t; if (e = e || {}, t = t || "img", d && !e.skipTest || !m) return !1; "img" === t ? t = document.getElementsByTagName("img") : "string" == typeof t ? t = document.querySelectorAll(t) : "length" in t || (t = [t]); for (var r = 0; r < t.length; r++)t[r][l] = t[r][l] || { skipTest: e.skipTest }, c(t[r]); i && (document.body.addEventListener("load", function (t) { "IMG" === t.target.tagName && a(t.target, { skipTest: e.skipTest }) }, !0), y = !0, t = "img"), e.watchMQ && window.addEventListener("resize", a.bind(null, t, { skipTest: e.skipTest })) } var l = "fregante:object-fit-images", u = /(object-fit|object-position)\s*:\s*([-.\w\s%]+)/g, g = "undefined" == typeof Image ? { style: { "object-position": 1 } } : new Image, f = "object-fit" in g.style, d = "object-position" in g.style, m = "background-size" in g.style, p = "string" == typeof g.currentSrc, b = g.getAttribute, h = g.setAttribute, y = !1; return a.supportsObjectFit = f, a.supportsObjectPosition = d, o(), a }();
  objectFitImages();
}

jQuery.fn.outerHTML = function(s) {return s ? this.before(s).remove() : jQuery("<p>").append(this.eq(0).clone()).html();};
!function(t){var i=t(window);t.fn.visible=function(t,e,o){if(!(this.length<1)){var r=this.length>1?this.eq(0):this,n=r.get(0),f=i.width(),h=i.height(),o=o?o:"both",l=e===!0?n.offsetWidth*n.offsetHeight:!0;if("function"==typeof n.getBoundingClientRect){var g=n.getBoundingClientRect(),u=g.top>=0&&g.top<h,s=g.bottom>0&&g.bottom<=h,c=g.left>=0&&g.left<f,a=g.right>0&&g.right<=f,v=t?u||s:u&&s,b=t?c||a:c&&a;if("both"===o)return l&&v&&b;if("vertical"===o)return l&&v;if("horizontal"===o)return l&&b}else{var d=i.scrollTop(),p=d+h,w=i.scrollLeft(),m=w+f,y=r.offset(),z=y.top,B=z+r.height(),C=y.left,R=C+r.width(),j=t===!0?B:z,q=t===!0?z:B,H=t===!0?R:C,L=t===!0?C:R;if("both"===o)return!!l&&p>=q&&j>=d&&m>=L&&H>=w;if("vertical"===o)return!!l&&p>=q&&j>=d;if("horizontal"===o)return!!l&&m>=L&&H>=w}}}}(jQuery);

function renderRecaptcha(){
  if ( $('.mfp-content input.django-recaptcha-hidden-field').length ) {
    grecaptcha.execute(
      $('script[src*="https://www.google.com/recaptcha/api.js?render="]')[0].src.split('=')[1],
      {action: 'generic'}).then(function(token) {
      document.querySelectorAll('.mfp-content input.django-recaptcha-hidden-field').forEach(function (value) {
        value.value = token;
      });
      return token;
    })
  }
}

let __opts = {popup:{}}
__opts.popup.defaults = {
  tClose: 'Закрыть (Esc)',
  tLoading: 'Загрузка...',
  type: 'inline',
  fixedContentPos: true,
  fixedBgPos: true,
  image: {
    tError: 'Произошла ошибка при загрузке <a href="%url%">изображения</a>.'
  },
  ajax: {
    settings: null,
    cursor: 'isProcessed',
    tError: 'Ошибка при загрузке <a href="%url%">содержимого</a>'
  },
  /*closeMarkup: '<button title="%title%" type="button" class="mfp-close">sdsad;</button>',*/
  callbacks: {
    beforeOpen: function() {
      $('body').addClass('withModalOpen');
    },
    open: function() {
      $('table').not('.tableHolder table').wrap('<div class="tableHolder"></div>');
      renderRecaptcha();
      $('.mfp-content textarea').html('').focus().blur();
      initJsDate();

    },
    change: function() {
    },
    ajaxContentAdded: function() {
    },
    afterClose: function() {
      $('body').removeClass('withModalOpen');
    },
  },
  verticalFit: true,
  closeBtnInside: true,
  preloader: true,
  removalDelay: 500,
  mainClass:'mfp-move-horizontal'
};
__opts.popup.gallery = $.extend(true,{},__opts.popup.defaults,{
  gallery: {
    enabled: true,
    tPrev: 'Предыдущее (&larr;)',
    tNext: 'Следующее (&rarr;)',
    tCounter: '%curr% из %total%',
    preload: [1,2],
  },
  modal:false,
  type: 'image',
  closeBtnInside: false,
  mainClass:'mfp-move-horizontal mfp-mode-gallery'
});

function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
  cache: false,
  beforeSend: function(xhr, settings) {
    var csrftoken = $('[name="csrfmiddlewaretoken"]').val();
    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
      xhr.setRequestHeader("X-CSRFToken", csrftoken);
    }
  }
});

$('body').on('click','.closeBlock, .mfp-close, .actionMfpClose',function() {
  $.magnificPopup.close();
  return false;
});

var ajaxRequest = function(e) {
  var url, enctype, method;
  var gaCallback = '';
  var yaCallback = '';

  /*console.log(this,e);*/

  if (e.type=='submit')
  {
    url = this.action;
    enctype = this.enctype;
    method = this.method;
  }

  if (e.type=='click')
  {
    url = this.getAttribute('href');
    enctype = false;
    method = 'get';
  }

  if ($(this).is('[data-ga]') && $(this).is('[data-metrika]')) {
    gaCallback = $(this).data('ga');
    yaCallback = $(this).data('metrika');
  }

  var src = ($(this).data('request-source') || ('#'+url.split('#')[1])).trim();
  var trg = $(this).data('request-target');
  var url = url.split('#')[0];

  if (!src) {return}

  e.preventDefault();

  var owner = this;

  var data;

  method = (method||'get').toUpperCase();

  if (e.type=='submit')
  {
    if (method=='POST')
    {
      data = new FormData(this);
    }
    else
    {
      data = $(this).serializeArray();
    }
  }

  $(owner).addClass('isProcessing');
  $(trg=='@self'?owner:src).addClass('isWaiting');
  if(url)
  {

    $.ajax({url:url||window.location.href, processData:method=='POST'?false:true, contentType:false, cache:false, type:method, method:method, data:data}).done(function(data) {

      $(owner).removeClass('isProcessing');

      var html = $('<div>'+data+'</div>').find(src);
      if (!html.length)
      {
        html = "<div class='message message-error'>Ошибка при загрузке</div>"
      }

      switch (trg)
      {
        case '@modal':
        case '@popup':
        {
          var type = trg.split('@')[1];
          var param = $(owner).data('request-param');
          param = param ? param.split(';') : [];
          for (var i=0;i<param.length;i++)
          {
            param[i]=param[i].split(':');
            param[i]="data-param-"+param[i][0]+(param[i][1]?("='"+param[i][1]+"'"):'');
          }
          var options = {items: {src:$("<div class='popupBlock popupBlock-"+type+"' data-request-source='"+src+"' data-request-url='"+url+"' "+($(owner).data('request-title')?('data-request-title="'+$(owner).data('request-title')+'"'):'')+(param.length?param.join(' '):'')+"><div class='blockWrapper'>"+($(owner).data('request-title')?("<h2 class='blockTitle'>"+$(owner).data('request-title')+"</h2>"):'')+"<div class='blockContent'>"+html.outerHTML()+"</div></div></div>")},type:'inline'};
          var magnificPopup = $.magnificPopup.instance;

          $.magnificPopup.open($.extend(true,{},__opts.popup.defaults,options));

          if (gaCallback.length && yaCallback.length) {
            $('.mfp-content').attr('data-metrika',yaCallback);
            $('.mfp-content').attr('data-ga',gaCallback);
          }
          $('.mfp-content textarea').text('')
          $('.mfp-content textarea').focus()
          $('.mfp-content textarea').parent().focus();
          /*if (formAjax) {
            $('.mfp-content form').prepend(inputsHidden);
            $('.mfp-content form').prepend(info);
          }*/

          break;
        }
        case '@blank':
        {
          var w = window.open();
          w.document.write(html.html());
          w.document.close();
          break;
        }
        case '@reload':
        {
          $.magnificPopup.close();
          if ($(owner).data('href') == undefined) {
            window.location.reload();
          } else {
            document.location.href = $(owner).data('href');
          }
          break;
        }
        default:
        {
          $(trg=='@self'?owner:src)[$(this).data('request-insert')?($(this).data('request-insert')=='prepend'?'prepend':'append'):'html'](html.html()).removeClass('isWaiting').addClass('isProcessed');

          /* update content behavior */
          $('table').not('.tableHolder table').wrap('<div class="tableHolder"></div>');
          $(window).scroll();
          $('.field :input').blur();
        }
      }
      if ($('.mfp-content .success_post_message').length) {
        metrickAdd($('.mfp-content').attr('data-metrika'),$('.mfp-content').attr('data-ga'));
      }
      $('input[type=radio]').on('change',function() {
        $('input[name='+$(this).attr("name")+']').removeAttr('checked');
      });
    });
  }
  else
  {
    var type = trg.split('@')[1];
    var param = $(owner).data('request-param');
    param = param ? param.split(';') : [];
    for (var i=0;i<param.length;i++)
    {
      param[i]=param[i].split(':');
      param[i]="data-param-"+param[i][0]+(param[i][1]?("='"+param[i][1]+"'"):'');
    }
    var options = {items: {src:$("<div class='popupBlock popupBlock-"+type+"' data-request-source='"+src+"' data-request-url='"+url+"' "+(param.length?param.join(' '):'')+"><div class='blockWrapper'>"+($(owner).data('request-title')?("<h2 class='blockTitle'>"+$(owner).data('request-title')+"</h2>"):'')+"<div class='blockContent'>"+$(src).html()+"</div></div></div>")},type:'inline'};
    $.magnificPopup.open($.extend(true,{},__opts.popup.defaults,options));
  }

  return false;
}

function percentInViewport (el,viewportLimit) {
  if (typeof jQuery === "function" && el instanceof jQuery) el = el.get(0);
  if (!el) {return false;}

  var wH = window.innerHeight || document.documentElement.clientHeight;
  var wW = window.innerWidth || document.documentElement.clientWidth;
  var rect = el.getBoundingClientRect();
  rect.h = rect.bottom-rect.top;
  rect.w = rect.right-rect.left;
  if (viewportLimit)
  {
    rect.h = Math.min(rect.h,wH);
    rect.w = Math.min(rect.w,wW);
  }
  return {
    vertical: +(( (Math.min(wH,Math.max(0,rect.bottom)) - Math.max(0,Math.min(wH,rect.top))) / rect.h ).toFixed(2)),
    horizontal: +(( (Math.min(wW,Math.max(0,rect.right)) - Math.max(0,Math.min(wW,rect.left))) / rect.w ).toFixed(2)),
  };
}

function SwiperInit(ListBlock, min) {
  const arrow = $(ListBlock).is('[data-arrow]');
  const pagination = $(ListBlock).is('[data-pagination]');
  const clickSlide = $(ListBlock).is('[data-click-slide]');
  if (min === undefined) {
    min = 1
  }
  $(ListBlock+':not(.initSlider)').each(function() {
    const owner = $('> ul',this);
    if ($('>li', owner).length > min) {
      $(ListBlock).addClass('initSlider')
      $(owner).wrap('<div class="swiperPlace"><div class="swiperOverflow"><div class="swiperWrapper"></div></div></div>');
      const place = $('.swiperWrapper',this);
      const autoPlay = $(owner).data('swiper-autoplay') !== undefined ? ''+$(owner).data('swiper-autoplay') : '';
      console.log(autoPlay.length)

      $(place).addClass("swiper-container");
      $(owner).addClass("swiper-wrapper");
      $('>li', owner).addClass("swiper-slide");
      if (pagination) {
        $('.swiperPlace',this).append('<div class="paginationSlider"><div class="paginationSliderWrapper"><div class="paginationSliderList"></div></div></div>');
      }
      if (arrow) {
        $('.swiperPlace',this).append('<div class="prev-slide arrowCustom"></div><div class="next-slide arrowCustom"></div>');
      }
      let sw = new Swiper(place, {
        slidesPerView: 'auto',
        /*loopedSlides: 4,
        loop: false,*/
        slideToClickedSlide: clickSlide,
        navigation: {
          nextEl: ListBlock+' .next-slide',
          prevEl: ListBlock+' .prev-slide',
        },
        autoplay: autoPlay.length ? {
          delay: autoPlay,
          disableOnInteraction: false,
        } : false,
        breakpoints: {
          1124: {
            /*loopedSlides: 4,
            centeredSlides: true,
            loop: true,*/
          },
          320: {
            /*loop: true,
            centeredSlides: false,
            loopedSlides: 2,*/
          }
        },
        pagination: pagination ? {
          el: '.paginationSliderList',
          clickable: true,
          renderBullet: function (index, className) {
            return '<div class="itemBullet '+className+'"><span></span></div>';
          },
        } : false
      });
      if (autoPlay.length) {
        $(this).on('mouseenter', function (e) {
          sw.autoplay.stop();
        })
        $(this).on('mouseleave', function (e) {
          sw.autoplay.start();
        })
      }
    }
  });
  return true;
}
function initJsDate() {
  $('input[type="date"]:not(.initDate)').attr('type', 'text').addClass('datePicker')
  $('input.datePicker:not(.initDate)').datepicker();
  $('input.datePicker:not(.initDate)').addClass('initDate');
}
function initSliders() {
  SwiperInit('.frame-sliderSmall .newsListBlock:not(.notJs)', false, true);
  SwiperInit('.frame-sliderSmall .profitListBlock:not(.notJs)', false, true);
  SwiperInit('.frame-sliderSmall .careListBlock:not(.notJs)', false, true);
  SwiperInit('.sliderFullBlock .sliderListBlock:not(.notJs)');
  SwiperInit('.imageGallery .imagesListBlock:not(.notJs)');
  SwiperInit('.frame-history .historyListBlock:not(.notJs)');
  SwiperInit('.frame-map .brandsContactWrapper:not(.notJs)',3);
}

$(function () { // ready start
  initSliders();
  $('header').on('click', '.item.sub>.action,.item.subjs>.action', function() {
    if ($('header .menuBlock').is(':visible')) {
      console.log('visible');
      $(this).parent().toggleClass('open');
    } else {
      if ($(this).parent().is('.subjs')) {
        console.log('subjs');
        //
        const owner = $(this).parent();
        const htmlSub = $('.navigationSub',owner).html();
        const $headerJsWrapper = $('header .headerContentJsWrapper');
        const $headerJs = $('header .headerContentJs');
        $(owner).toggleClass('open');
        if ($(owner).is('.open')) {
          $headerJs.addClass('open');
          $headerJsWrapper.html(htmlSub)
        } else {
          $headerJs.removeClass('open');
          $headerJsWrapper.html('');
        }
      }
    }
    return false;
  });

  if ($.fn.mask) {
    $(document).on('focus','input[type="tel"],input.phone',function(){
      $(this).mask('+7 (999) 999-99-99');
    })
  }

  $('.mainAside aside').each(function() {

    const $owner = $(this);
    const main = $owner.closest('.mainAside');
    $(window).resize(function () {
      //console.log($owner.is(":visible"));
      if ($owner.is(":visible")) {
        window.removeEventListener('scroll', $owner.stick);
        // const headerHeight = $('header').height();
        $owner.stick = function () {
          const startCord = $(main).offset().top;//$owner.offset().top; //- headerHeight;
          const endCord = ( ($(main).offset().top + $(main).height()) )  - $owner.height();
          if ($owner.is(":visible")) {
            if ($(document).scrollTop() > startCord && $(document).scrollTop() < endCord) {
              $owner.addClass('fixed');
            } else {
              $owner.removeClass('fixed');
            }
          }
        };
        window.addEventListener('scroll', $owner.stick);
      }
    }).resize();
  });

  $('.frame-detailProject .rollBlock').on('click',' .captionBlock', function() {
    $(this).closest('.rollBlock').toggleClass('open');
  });

  $('.filterBlock').on('click','.searchButton', function() {
    const $parent = $(this).parent();
    $parent.toggleClass('open');
    $parent.hasClass('open')?setTimeout(function () { $('input.search',$parent.parent()).get(0).focus()}, 400):"";
    return false;
  });


  $('.textBlock-hide').on('click', '.action-readMore', function() {
    const $owner = $(this).closest('.textBlock-hide');
    $owner.toggleClass('visible');
    $owner.hasClass('visible')?$(this).text($(this).data('hide')):$(this).text($(this).data('visible'));
  })



  $('body a[href*="#"]:not([href="#"]):not(.notJs)').on('click',function(e) {
    const el = ('#'+$(this).attr('href').split('#')[1]);
    $('body,.menuSwitch').removeClass('openMenu');
    if ($(el).length) {
      const elHeight = $(el).offset().top;// - $('header').height();
      $('html,body').animate(
        {scrollTop: elHeight},
        1000,
        console.log(''));
      return false;
    }
  });

  $('body').on('submit','form[data-request-source]',ajaxRequest);
  $('body').on('click','a[data-request-source]',ajaxRequest);
  $(document).on('change keyup','form.filter :input',function() {
    if ($(this).is('[data-onchange="skip"]')) {return}

    var form = this.form;
    if ($(this).is('[data-onchange="delay"],[data-onchange^="delay:"]'))
    {
      if (this.__onchangeTimer) {window.clearTimeout(this.__onchangeTimer);}
      var delay = parseFloat($(this).data('onchange').split(':')[1]) || 1;
      this.__onchangeTimer = window.setTimeout(function(){$(form).trigger('submit')},delay*1000);
    }
    else
    {
      $(form).trigger('submit');
    }
  })

  $('body').on('click','a.paginationCaption', function(e) {
    if (!$(this).hasClass('process')) {
      const action =  $(this);
      $(action).addClass('process');
      const src = $(this).data('src');
      $.ajax({url:$(this).attr('href'), processData:true, contentType:false, cache:false, type:'get', method:'get'}).done(function(data) {
        const html = $('<div>' + data + '</div>').find(src);
        console.log($('<div>' + data + '</div>').find('a.paginationCaption').attr('href'));
        if ($('<div>' + data + '</div>').find('a.paginationCaption').length) {
          $(action).attr('href', $('<div>' + data + '</div>').find('a.paginationCaption').attr('href'))
        } else {
          $(action).parent().remove();
        }
        $(src).append(html.html());
        $(action).removeClass('process');
      });
    }
    return false;
  });

  $('body').on('click','.menuSwitch',function(e) {
    e.preventDefault();
    if ($('body').hasClass('menuSwitched-inner'))
    {
      $('body').removeClass('menuSwitched-inner');
    }
    else
    {
      $(this).toggleClass('openMenu');
      $('body').toggleClass('openMenu');
    }
    return false;
  });

  $(document).on('click','[data-videoonly], [data-imageonly]',function(e){
    if ($.fn.magnificPopup)
    {
      e.preventDefault();
      if ($(this).is('[data-videoonly]')) {
        const options = $.extend(true,{},__opts.popup.gallery,{items:[{src:this.href,type:'iframe'}]});
        $.magnificPopup.open(options,0);
      } else {
        const options = $.extend(true,{},__opts.popup.gallery,{items:[{src:this.href,type:'image'}]});
        $.magnificPopup.open(options,0);
      }
      return false;
    }
  });


  let map;

  mapInit = function() {

    $('.frame-map #map').each(function() {
      let image = iconMapInit();
      const owner = this;
      const frame = $(owner).closest('.frame');

      const $descriptionBlock = $('.descriptionBlock',frame);
      const $brandsBlock = $('.brandContactListBlock',frame);



      let time = setInterval(
        function () {
          $(this).addClass('hover');
        },2000
      )

      $(this).mousedown(function() {
        $('#map').addClass('hover');
      }).mouseleave(function() {
        $('#map').removeClass('hover');
      });

      map = new ymaps.Map('map', {
        center: [50, 30],
        zoom: 3,
        controls: ['typeSelector','zoomControl']
      }, {
        typeSelectorSize: 'small',
        autoFitToViewport: 'always'
      });

      if (!!('ontouchstart'in window) || !!('msmaxtouchpoints'in window.navigator)) {
        map.behaviors.disable('drag');
      } else {
        map.behaviors.disable('scrollZoom');
      }
      map.panes.get('ground').getElement().style.filter = 'grayscale(100%)'; // invert(1)
      //console.log(map.panes.get('ground').getElement());
      let collection = [];
      $('.mapPointsList>.item[data-coord]',frame).each(function() {
        const item = this;
        let coord = this.getAttribute('data-coord')
        if (!coord) {
          return false;
        }
        coord = coord.split(',');
        //console.log(coord);
        const point = new ymaps.Placemark([coord[0],coord[1]], {
            iconContent: '',
            balloonContent: '',
          },
          {
            iconLayout: 'default#image',
            iconImageHref: $(item).hasClass('active')?image.hover:image.default, // "data:image/svg+xml,<svg width='26' height='36' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M12.85 0A12.87 12.87 0 000 12.85c0 8.8 11.5 21.71 12 22.26.45.51 1.26.51 1.71 0 .5-.55 12-13.46 12-22.26C25.7 5.77 19.94 0 12.85 0zm0 19.32a6.47 6.47 0 11.02-12.95 6.47 6.47 0 01-.02 12.95z' fill='%2300AEEF'/></svg>"
            iconImageSize: image.iconImageSize,
            iconImageOffset: image.iconImageOffset,
            balloonAutoPan: true,
            balloonCloseButton: false,
            balloonMinWidth: 50,
            hideIconOnBalloonOpen: false,
            _brands: $('ul.brandContactList',item).outerHTML(),
            _description: $('.contentBlock .description',item).outerHTML(),
            _active: $(item).hasClass('active'),
            //__opened: false
          }
        );
        collection.push(point);
        map.geoObjects
          .add(point)
      })
      var pointsCollection = ymaps.geoQuery(map.geoObjects);
      //console.log(pointsCollection);
      if ( $('.mapPointsList>.item[data-coord]',frame).length>1) {
        map.geoObjects.events.add('click', function (e) {
          const contentBrands = e.get('target').options._options._brands;
          const contentDescription = e.get('target').options._options._description;
          /*console.log(e.get('target').options._options._brands);   //.set('preset', 'islands#redIcon');
          console.log(e.get('target').options._options._description); //.set('preset', 'islands#redIcon');*/
          $descriptionBlock.html(contentDescription)
          $brandsBlock.html('<div class="brandsContactWrapper" data-arrow>' + contentBrands + '</div>')
          if (SwiperInit('.frame-map .brandsContactWrapper:not(.notJs)', true, 3)) {
            $('.asideMap .brandContactList .brandContact:first').click();
          }
        })
      }
      map.geoObjects.events.add('mouseleave',function(e) {
        // console.log(e.get('target').options.get('_active'));
        if (!e.get('target').options.get('_active')) {
          $(e.get('target').options.set('iconImageHref', image.default))
        }
      }).add('mouseenter',function(e) {
        if (!e.get('target').options.get('_active')) {
          $(e.get('target').options.set('iconImageHref', image.hover))
        }
      }).add('mousedown',function(e) {
        for (let i = 0; i < pointsCollection.getLength(); i++) {
          const point = pointsCollection.get(i);
          point.options.set('iconImageHref', image.default);
        }
        $(e.get('target').options.set('iconImageHref', image.hover))
        $(e.get('target').options.set('_active', true))
      });


      if ( $('.mapPointsList>.item[data-coord]',frame).length > 1) {
        map.setBounds(map.geoObjects.getBounds(),{checkZoomRange:true, zoomMargin:9});
      } else {
        map.setBounds(map.geoObjects.getBounds(),{checkZoomRange:false, zoomMargin:15});
        map.setZoom(15);
      }


      console.log(map.geoObjects);
    });

    mapContactInit();

  }
  function iconMapInit() {
    const images = {
      small: {
        default: 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDMiIGhlaWdodD0iNDMiIHZpZXdCb3g9IjAgMCA0MyA0MyIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMzAuNzUgMTkuODMzNEMzMC43NSAyNy40MTY3IDIxIDMzLjkxNjcgMjEgMzMuOTE2N0MyMSAzMy45MTY3IDExLjI1IDI3LjQxNjcgMTEuMjUgMTkuODMzNEMxMS4yNSAxNy4yNDc1IDEyLjI3NzIgMTQuNzY3NiAxNC4xMDU3IDEyLjkzOTFDMTUuOTM0MiAxMS4xMTA2IDE4LjQxNDEgMTAuMDgzNCAyMSAxMC4wODM0QzIzLjU4NTkgMTAuMDgzNCAyNi4wNjU4IDExLjExMDYgMjcuODk0MyAxMi45MzkxQzI5LjcyMjggMTQuNzY3NiAzMC43NSAxNy4yNDc1IDMwLjc1IDE5LjgzMzRaIiBzdHJva2U9InVybCgjcGFpbnQwX2xpbmVhcikiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIi8+PHBhdGggZD0iTTIxIDIzLjA4MzRDMjIuNzk0OSAyMy4wODM0IDI0LjI1IDIxLjYyODMgMjQuMjUgMTkuODMzNEMyNC4yNSAxOC4wMzg0IDIyLjc5NDkgMTYuNTgzNCAyMSAxNi41ODM0QzE5LjIwNTEgMTYuNTgzNCAxNy43NSAxOC4wMzg0IDE3Ljc1IDE5LjgzMzRDMTcuNzUgMjEuNjI4MyAxOS4yMDUxIDIzLjA4MzQgMjEgMjMuMDgzNFoiIHN0cm9rZT0idXJsKCNwYWludDFfbGluZWFyKSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiLz48ZGVmcz48bGluZWFyR3JhZGllbnQgaWQ9InBhaW50MF9saW5lYXIiIHgxPSIyOS44MDk4IiB5MT0iNC4yMzMzNiIgeDI9IjcuMzk1NSIgeTI9IjUuMjY2OSIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiPjxzdG9wIHN0b3AtY29sb3I9IiNDRDIwMkMiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNGRjMzNDEiLz48L2xpbmVhckdyYWRpZW50PjxsaW5lYXJHcmFkaWVudCBpZD0icGFpbnQxX2xpbmVhciIgeDE9IjIzLjkzNjYiIHkxPSIxNC45ODc5IiB4Mj0iMTYuNDczIiB5Mj0iMTUuNDA4NSIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiPjxzdG9wIHN0b3AtY29sb3I9IiNDRDIwMkMiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNGRjMzNDEiLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48L3N2Zz4=',
        hover: 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDMiIGhlaWdodD0iNDMiIHZpZXdCb3g9IjAgMCA0MyA0MyIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48Y2lyY2xlIGN4PSIyMS41IiBjeT0iMjEuNSIgcj0iMjEuNSIgZmlsbD0idXJsKCNwYWludDBfbGluZWFyKSIvPjxwYXRoIGQ9Ik0zMC43NSAxOS44MzM0QzMwLjc1IDI3LjQxNjcgMjEgMzMuOTE2NyAyMSAzMy45MTY3QzIxIDMzLjkxNjcgMTEuMjUgMjcuNDE2NyAxMS4yNSAxOS44MzM0QzExLjI1IDE3LjI0NzUgMTIuMjc3MiAxNC43Njc2IDE0LjEwNTcgMTIuOTM5MUMxNS45MzQyIDExLjExMDYgMTguNDE0MSAxMC4wODM0IDIxIDEwLjA4MzRDMjMuNTg1OSAxMC4wODM0IDI2LjA2NTggMTEuMTEwNiAyNy44OTQzIDEyLjkzOTFDMjkuNzIyOCAxNC43Njc2IDMwLjc1IDE3LjI0NzUgMzAuNzUgMTkuODMzNFoiIHN0cm9rZT0id2hpdGUiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIi8+PHBhdGggZD0iTTIxIDIzLjA4MzRDMjIuNzk0OSAyMy4wODM0IDI0LjI1IDIxLjYyODMgMjQuMjUgMTkuODMzNEMyNC4yNSAxOC4wMzg0IDIyLjc5NDkgMTYuNTgzNCAyMSAxNi41ODM0QzE5LjIwNTEgMTYuNTgzNCAxNy43NSAxOC4wMzg0IDE3Ljc1IDE5LjgzMzRDMTcuNzUgMjEuNjI4MyAxOS4yMDUxIDIzLjA4MzQgMjEgMjMuMDgzNFoiIHN0cm9rZT0id2hpdGUiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIi8+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJwYWludDBfbGluZWFyIiB4MT0iNDAuOTI2OCIgeTE9Ii0xMC41NTQ2IiB4Mj0iLTguNDQ3OTQiIHkyPSItNy43NzE5MiIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiPjxzdG9wIHN0b3AtY29sb3I9IiNDRDIwMkMiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNGRjMzNDEiLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48L3N2Zz4=',
        iconImageSize: [43, 43],
        iconImageOffset: [-21.5, -33],
      },
      big: {
        default: 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTA0IiBoZWlnaHQ9IjEwNCIgdmlld0JveD0iMCAwIDEwNCAxMDQiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTTcwIDQ4QzcwIDYyIDUyIDc0IDUyIDc0QzUyIDc0IDM0IDYyIDM0IDQ4QzM0IDQzLjIyNjEgMzUuODk2NCAzOC42NDc3IDM5LjI3MjEgMzUuMjcyMUM0Mi42NDc3IDMxLjg5NjQgNDcuMjI2MSAzMCA1MiAzMEM1Ni43NzM5IDMwIDYxLjM1MjMgMzEuODk2NCA2NC43Mjc5IDM1LjI3MjFDNjguMTAzNiAzOC42NDc3IDcwIDQzLjIyNjEgNzAgNDhaIiBzdHJva2U9InVybCgjcGFpbnQwX2xpbmVhcikiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIi8+PHBhdGggZD0iTTUyIDU0LjAwMDJDNTUuMzEzNyA1NC4wMDAyIDU4IDUxLjMxNCA1OCA0OC4wMDAyQzU4IDQ0LjY4NjUgNTUuMzEzNyA0Mi4wMDAyIDUyIDQyLjAwMDJDNDguNjg2MyA0Mi4wMDAyIDQ2IDQ0LjY4NjUgNDYgNDguMDAwMkM0NiA1MS4zMTQgNDguNjg2MyA1NC4wMDAyIDUyIDU0LjAwMDJaIiBzdHJva2U9IndoaXRlIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIvPjxwYXRoIGQ9Ik01MiA1NEM1NS4zMTM3IDU0IDU4IDUxLjMxMzcgNTggNDhDNTggNDQuNjg2MyA1NS4zMTM3IDQyIDUyIDQyQzQ4LjY4NjMgNDIgNDYgNDQuNjg2MyA0NiA0OEM0NiA1MS4zMTM3IDQ4LjY4NjMgNTQgNTIgNTRaIiBzdHJva2U9InVybCgjcGFpbnQxX2xpbmVhcikiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIi8+PGRlZnM+PGxpbmVhckdyYWRpZW50IGlkPSJwYWludDBfbGluZWFyIiB4MT0iNjguMjY0MyIgeTE9IjE5LjIiIHgyPSIyNi44ODQiIHkyPSIyMS4xMDgxIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSI+PHN0b3Agc3RvcC1jb2xvcj0iI0NEMjAyQyIvPjxzdG9wIG9mZnNldD0iMSIgc3RvcC1jb2xvcj0iI0ZGMzM0MSIvPjwvbGluZWFyR3JhZGllbnQ+PGxpbmVhckdyYWRpZW50IGlkPSJwYWludDFfbGluZWFyIiB4MT0iNTcuNDIxNCIgeTE9IjM5LjA1NDUiIHgyPSI0My42NDI0IiB5Mj0iMzkuODMxMSIgZ3JhZGllbnRVbml0cz0idXNlclNwYWNlT25Vc2UiPjxzdG9wIHN0b3AtY29sb3I9IiNDRDIwMkMiLz48c3RvcCBvZmZzZXQ9IjEiIHN0b3AtY29sb3I9IiNGRjMzNDEiLz48L2xpbmVhckdyYWRpZW50PjwvZGVmcz48L3N2Zz4=',
        hover: 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTA0IiBoZWlnaHQ9IjEwNCIgdmlld0JveD0iMCAwIDEwNCAxMDQiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGNpcmNsZSBjeD0iNTIiIGN5PSI1Mi4wMDAyIiByPSI1MiIgZmlsbD0idXJsKCNwYWludDBfbGluZWFyKSIvPjxwYXRoIGQ9Ik03MCA0OC4wMDAyQzcwIDYyLjAwMDIgNTIgNzQuMDAwMiA1MiA3NC4wMDAyQzUyIDc0LjAwMDIgMzQgNjIuMDAwMiAzNCA0OC4wMDAyQzM0IDQzLjIyNjMgMzUuODk2NCAzOC42NDggMzkuMjcyMSAzNS4yNzIzQzQyLjY0NzcgMzEuODk2NyA0Ny4yMjYxIDMwLjAwMDIgNTIgMzAuMDAwMkM1Ni43NzM5IDMwLjAwMDIgNjEuMzUyMyAzMS44OTY3IDY0LjcyNzkgMzUuMjcyM0M2OC4xMDM2IDM4LjY0OCA3MCA0My4yMjYzIDcwIDQ4LjAwMDJaIiBzdHJva2U9IndoaXRlIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIvPjxwYXRoIGQ9Ik01MiA1NC4wMDAyQzU1LjMxMzcgNTQuMDAwMiA1OCA1MS4zMTQgNTggNDguMDAwMkM1OCA0NC42ODY1IDU1LjMxMzcgNDIuMDAwMiA1MiA0Mi4wMDAyQzQ4LjY4NjMgNDIuMDAwMiA0NiA0NC42ODY1IDQ2IDQ4LjAwMDJDNDYgNTEuMzE0IDQ4LjY4NjMgNTQuMDAwMiA1MiA1NC4wMDAyWiIgc3Ryb2tlPSJ3aGl0ZSIgc3Ryb2tlLXdpZHRoPSIyIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1saW5lam9pbj0icm91bmQiLz48ZGVmcz48bGluZWFyR3JhZGllbnQgaWQ9InBhaW50MF9saW5lYXIiIHgxPSI5OC45ODU3IiB5MT0iLTI1LjUyNzEiIHgyPSItMjAuNDMyMiIgeTI9Ii0xOC43OTciIGdyYWRpZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIj48c3RvcCBzdG9wLWNvbG9yPSIjQ0QyMDJDIi8+PHN0b3Agb2Zmc2V0PSIxIiBzdG9wLWNvbG9yPSIjRkYzMzQxIi8+PC9saW5lYXJHcmFkaWVudD48L2RlZnM+PC9zdmc+',
        iconImageSize: [104, 104],
        iconImageOffset: [-52, -74],
      }
    }
    let image = false;

    if ($('header .menuBlock').is(':visible')) {
      return images.small;
    } else {
      return  images.big;
    }
    return false
  }
  function mapContactInit() {
    $('.mapContactBlock #map:visible:not(.mapInit)').each(function() {


      image = iconMapInit();
      if ($('.mapContactList .contact').length) {
        const owner = this;

        let time = setInterval(
          function () {
            $(this).addClass('hover');
          }, 2000
        )

        $(this).mousedown(function () {
          $('#map').addClass('hover');
        }).mouseleave(function () {
          $('#map').removeClass('hover');
        });

        map = new ymaps.Map('map', {
          center: [50, 30],
          zoom: 3,
          controls: ['typeSelector','zoomControl']
        }, {
          typeSelectorSize: 'small',
          autoFitToViewport: 'always'
        });

        if (!!('ontouchstart'in window) || !!('msmaxtouchpoints'in window.navigator)) {
          map.behaviors.disable('drag');
        } else {
          map.behaviors.disable('scrollZoom');
        }

        map.panes.get('ground').getElement().style.filter = 'grayscale(100%)';

        $('.mapContactList>.contact[data-coord]').each(function () {
          const item = this;

          const htmlContent = $('.contactEntryBlock',this).outerHTML();

          let coord = this.getAttribute('data-coord')
          if (!coord) {
            return false;
          }
          coord = coord.split(',');
          console.log(coord);
          const point = new ymaps.Placemark([coord[0], coord[1]], {
              iconContent: '',
              balloonContent: htmlContent,
            },
            {
              iconLayout: 'default#image',
              iconImageHref: image.default, // "data:image/svg+xml,<svg width='26' height='36' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M12.85 0A12.87 12.87 0 000 12.85c0 8.8 11.5 21.71 12 22.26.45.51 1.26.51 1.71 0 .5-.55 12-13.46 12-22.26C25.7 5.77 19.94 0 12.85 0zm0 19.32a6.47 6.47 0 11.02-12.95 6.47 6.47 0 01-.02 12.95z' fill='%2300AEEF'/></svg>"
              iconImageSize: image.iconImageSize,
              iconImageOffset: image.iconImageOffset,
              balloonAutoPan: true,
              balloonCloseButton: true,
              balloonMinWidth: 50,
              hideIconOnBalloonOpen: false,
              _brands: $('ul.brandContactList', item).outerHTML(),
              _description: $('.contentBlock .description', item).outerHTML(),
              //__opened: false
            }
          );

          map.geoObjects
            .add(point)
        })

        map.geoObjects.events.add('mouseleave', function (e) {
          $(e.get('target').options.set('iconImageHref', image.default))
        }).add('mouseenter', function (e) {
          $(e.get('target').options.set('iconImageHref', image.hover))
        })
        if ($('.mapContactList>.contact[data-coord]').length > 1) {
          map.geoObjects.events.add('click', function (e) {
            console.log('click map');
          });
        }

        map._center = function() {
          console.log(map.geoObjects.getBounds());
          map.setBounds(map.geoObjects.getBounds(),{checkZoomRange:true, zoomMargin:9});
        }
        map._center();
      }
      $(this).addClass('mapInit')
    });
  }



  $('.asideMap').each(function() {
    const owner = this;
    $(this).on('click', '.brandContactList .brandContact', function() {
      const list = $(this).parent();
      $('.brandContact',list).removeClass('active');
      $(this).addClass('active');
      $('.asideMapWrapper > .contactsContent',owner).html($('.contactsContent .contactsWrapper',this).outerHTML());
      $('.asideMapWrapper > .descriptionBlock',owner).html($('.descriptionBlock .description',this).outerHTML());
    });
  })

  $(document).on('click','.accordionList .item .actionMore',function() {
    const owner = $(this).closest('.item');
    $(owner).toggleClass('open');
    $(owner).hasClass('open') ? $('.caption',this).text($(this).data('visible')) : $('.caption',this).text($(this).data('hidden'));
  });

  $(document).on('click','.contentMoreBlock .actionMore',function() {
    const owner = $(this).closest('.contentMoreBlock');
    $(owner).toggleClass('open');
    $(owner).hasClass('open') ? $('.caption',this).text($(this).data('visible')) : $('.caption',this).text($(this).data('hidden'));
  });

  $('.tabsBox').on('click','.tabsList .action',function() {
    const owner = $(this).closest('.tabsBox');
    const list = $('.tabsList',owner);
    const id = $(this).attr('href');

    //tabs reset active
    $('.tabAction',list).removeClass('active');
    $(this).closest('.tabAction').addClass('active');

    //tabsHref visible
    $('.tabContent',owner).removeClass('active');
    $(id+'.tabContent',owner).addClass('active');
    if ($(id+'.tabContent #map:not(.mapInit)',owner).length) {
      console.log('mapcenter');
      $(id+'.tabContent #map').addClass('mapCenter');
      mapContactInit();
    }
    return false;
  });

  initJsDate();
  $('table').not('.tableHolder table').wrap('<div class="tableHolder"></div>');

  $('body').removeClass('jsLoading').addClass('jsLoaded');

  $('body').on('change', 'input[type="file"]', function (e) {
    console.log(e, this.files[0].name)
    $(this).parent('.fileLoadBox').find('.caption').html(this.files[0].name)
  })
  $('.cityActionBlock').on('click','.actionCity', function () {
    $(this).closest('.cityActionBlock').toggleClass('open');
    return false;
  });

  $('body').on('click','a.paginationCaption', function(e) {
    if (!$(this).hasClass('process')) {
      const action =  $(this);
      $(action).addClass('process');
      const src = $(this).data('src');
      $.ajax({url:$(this).attr('href'), processData:true, contentType:false, cache:false, type:'get', method:'get'}).done(function(data) {
        const html = $('<div>' + data + '</div>').find(src);
        console.log($('<div>' + data + '</div>').find('a.paginationCaption').attr('href'));
        if ($('<div>' + data + '</div>').find('a.paginationCaption').length) {
          $(action).attr('href', $('<div>' + data + '</div>').find('a.paginationCaption').attr('href'))
        } else {
          $(action).parent().remove();
        }
        $(src).append(html.html());
        $(action).removeClass('process');
      });
    }
    return false;
  });


}); // ready end
